<?php
$installer = $this;
$installer->startSetup();
$installer->run("ALTER TABLE {$this->getTable('adminlogger_log')} ADD COLUMN `al_ip_address` varchar(39);");
$installer->endSetup();