<?php

class MDN_BarcodeLabel_Helper_ShipmentLabel extends Mage_Core_Helper_Abstract {

    // apply coefficient to increase the resolution
    private $_coef = 2;
    private $_customCount = 3;
		private $_heightoffset = 30;

    /**
     * Return label image for shipment
     * @param <type> $productId
     */
    public function getImage($orderId) {
        #$product = Mage::getModel('catalog/product')->load($productId);
								
				$orderToPrepare = mage::getModel('Orderpreparation/ordertoprepare')->load($orderId, 'order_id');
							
        //create base image
        $labelSize = Mage::helper('BarcodeLabel')->getlabelSize();
        $height = $labelSize['height'] * $this->_coef;
        $width = $labelSize['width'] * $this->_coef;

        $im = imagecreatetruecolor($width, $height);
        $white = imagecolorallocate($im, 255, 255, 255);
        imagefilledrectangle($im, 0, 0, $width, $height, $white);

        //add barcode
        $this->addBarcode($im, $orderToPrepare);

        //add order ID
        $this->addOrderId($im, $orderToPrepare);

				//add shipping method
				$this->addShippingMethod($im, $orderToPrepare);
				
				//add packer ID
				$this->addPacker($im, $orderToPrepare);

        //return image
        return $im;
    }

    /**
     * Add barcode to img
     * @param <type> $im
     */
    protected function addBarcode(&$image, $orderToPrepare) {

        if (Mage::getStoreConfig('barcodelabel/barcode/print') != 1)
            return false;

        //get barcode image
				
				$shipmentId = $orderToPrepare->getshipment_id();
        if (!$shipmentId) {
					$shipmentId = '000000000';
				} 
				
				$barcodeImage = Mage::helper('BarcodeLabel/BarcodePicture')->getImage($shipmentId);

        $barcodeImageWidth = imagesx($barcodeImage);
        $barcodeImageHeight = imagesy($barcodeImage);

        $position = $this->getPositions(Mage::getStoreConfig('barcodelabel/barcode/position'), true);
        #if ($position['width'] == 0)
            #$position['width'] = $barcodeImageWidth * $this->_coef;
        #if ($position['height'] == 0)
        #    $position['height'] = $barcodeImageHeight * $this->_coef;

			  $position['width'] = 0;
				$position['height'] = 0;
				
				# override the x y position from the DB
				$position['x'] = 60;
				$position['y'] = -60 + $this->_heightoffset;
				
        //add barcode on the picture
        imagecopyresized($image, $barcodeImage, $position['x'], $position['y'], 0, 0, ($barcodeImageWidth * 2), ($barcodeImageHeight * 2), $barcodeImageWidth, $barcodeImageHeight);
				#imagecopy ( resource $dst_im , resource $src_im , int $dst_x , int $dst_y , int $src_x , int $src_y , int $src_w , int $src_h )
        #imagecopy ( $image, $barcodeImage, $position['x'], $position['y'], 0, 0, $barcodeImageWidth, $barcodeImageHeight );

				//imagecopyresized ($dst_image , $src_image , int $dst_x , int $dst_y , int $src_x , int $src_y , int $dst_w , int $dst_h , int $src_w , int $src_h )
    }

    /**
     *
     * @param <type> $im
     * @param <type> $product 
     */
    protected function addOrderId(&$im, $orderToPrepare) {

        if (Mage::getStoreConfig('barcodelabel/name/print') != 1)
            return false;

        #$name = $product->getName();
				$orderid = '00000000';
				$shipmentIncrementId = $orderToPrepare->getshipment_id();
				if ($shipmentIncrementId)
        {
            $shipment = Mage::getModel('sales/order_shipment')->load($shipmentIncrementId, 'increment_id');
        }
				if ($shipment) {
					$order = $shipment->getOrder();					
				}
				if ($order) {
					$orderid = $order->getincrement_id();					
				}

				$fontSize = Mage::getStoreConfig('barcodelabel/name/font_size') * $this->_coef;
        $position = $this->getPositions(Mage::getStoreConfig('barcodelabel/name/position'), true);
        $black = imagecolorallocate($im, 0, 0, 0);
        $font = $this->getFont();

				#override X and Y position
				$position['x'] = 10;
				$position['y'] = 80 + $this->_heightoffset;
        
				#$orderid = $this->truncateToSize($orderid, $position['width'], $font, $fontSize);
				$orderid = "Order Number: " . $orderid;
        imagettftext($im, $fontSize, 0, $position['x'], $position['y'], $black, $font, $orderid);
    }

    /**
     *
     * @param <type> $im
     * @param <type> $product 
     */
    protected function addShippingMethod(&$im, $orderToPrepare) {

        if (Mage::getStoreConfig('barcodelabel/name/print') != 1)
            return false;

        #$name = $product->getName();
        $shipmethod = $orderToPrepare->getshipping_method();
				$fontSize = Mage::getStoreConfig('barcodelabel/name/font_size') * $this->_coef;
        $position = $this->getPositions(Mage::getStoreConfig('barcodelabel/name/position'), true);
        $black = imagecolorallocate($im, 0, 0, 0);
        $font = $this->getFont();

				#override X and Y position
				$position['x'] = 10;
				$position['y'] = 100 + $this->_heightoffset;
        
				$shipmethod = $this->truncateToSize($shipmethod, $position['width'], $font, $fontSize);
        imagettftext($im, $fontSize, 0, $position['x'], $position['y'], $black, $font, $shipmethod);
    }

   /**
     *
     * @param <type> $im
     * @param <type> $product 
     */
    protected function addPacker(&$im, $orderToPrepare) {

        if (Mage::getStoreConfig('barcodelabel/name/print') != 1)
            return false;

        #$name = $product->getName();
        $packer = $orderToPrepare->getuser();
				$fontSize = 14 * $this->_coef;
        $position = $this->getPositions(Mage::getStoreConfig('barcodelabel/name/position'), true);
        $black = imagecolorallocate($im, 0, 0, 0);
        $font = $this->getFont();

				#override X and Y position
				$position['x'] = 20;
				$position['y'] = 30 + $this->_heightoffset;
        
				imagettftext($im, $fontSize, 0, $position['x'], $position['y'], $black, $font, $packer);
    }

    /**
     * Convert cm to point
     * @param <type> $cm
     * @return <type>
     */
    protected function convertCmToPoint($cm) {
        return $cm * 50;
    }

    /**
     *
     */
    public function getPositions($positionString, $convertToPoint) {
        $t = explode(',', $positionString);

        $positions = array();
        $positions['x'] = $t[0];
        $positions['y'] = $t[1];
        $positions['width'] = $t[2];
        $positions['height'] = $t[3];

        if ($convertToPoint) {
            foreach ($positions as $k => $value) {
                $positions[$k] = $this->convertCmToPoint($value) * $this->_coef;
            }
        }

        return $positions;
    }

    /**
     * Return font
     * @return string
     */
    protected function getFont() {
        $path = Mage::getBaseDir() . '/media/font/' . Mage::getStoreConfig('barcodelabel/label/font');
        return $path;
    }

    /**
     *
     * @param type $text
     * @param type $maxWidth : position X
     * @param type $font
     * @param type $size
     * @return type 
     */
    protected function truncateToSize($text, $maxWidth, $font, $size) {

            $dimensions = imagettfbbox($size, null, $font, $text); 
            $realWidth = $dimensions[2] - $dimensions[0]; // image width
            $ratio = 219; // ratio found when $maxWidth = strlen($text)
            $maxLenght = $maxWidth - $ratio;
            
            if ($realWidth >= $maxWidth) {          
                $param = substr($text, 0, $maxLenght); 
                $text = wordwrap($text, strlen($param), "\n");
            }

        return $text;
    }
    
    
}
