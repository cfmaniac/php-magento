<?php

$installer=$this;
$installer->startSetup();

$installer->getConnection()->addColumn($installer->getTable('order_to_prepare'), 'sent_to_shipworks', 'TINYINT NOT NULL DEFAULT 0');

$installer->endSetup();

