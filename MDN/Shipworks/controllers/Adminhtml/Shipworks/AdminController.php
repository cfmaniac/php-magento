<?php

class MDN_Shipworks_Adminhtml_Shipworks_AdminController extends Mage_Adminhtml_Controller_Action {
    
    /**
     * Reset downloaded in shipworks status for shipment
     */
    public function ResetAction()
    {
       $shipmentIncrementId = $this->getRequest()->getParam('id');

       if($shipmentIncrementId != null) {
            Mage::helper('Shipworks/Shipments')->flagAsNotSent($shipmentIncrementId);
       }
       
       $this->_redirectReferer();
    }

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/system/config/shipworks');
    }

}
