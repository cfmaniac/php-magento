<?php

class MDN_Shipworks_Helper_Log extends Mage_Core_Helper_Abstract {
    

    public function Log($inputData, $output, $errorMessage)
    {
        //set path
        $dirPath = $filePath = Mage::getBaseDir('var').DS.'ShipworksLog'.DS;

        if (!is_dir($dirPath)) {
            mkdir($dirPath);
        }
        $htaccessPath = $dirPath.'.htaccess';
        if (!file_exists($htaccessPath)) {
            file_put_contents($htaccessPath, "Order deny,allow\nDeny from all\n");
        }

        $filePath = $dirPath.date('YmdHis').'.log';
        
        //log
        $inputDataString = "\r\n#############################################";
        $inputDataString .= "\r\nINPUT DATA : ";

        foreach($inputData as $k => $v){
            if ('password' == $k) {
                $v = '********';
            } elseif ('username' == $k) {
                $v = substr($v,0,2).'****';
            }
            $inputDataString .= "\r\n".$k.' = '.$v;
        }

        $inputDataString .= "\r\n#############################################";
        $inputDataString .= "\r\nOUTPUT : ";

        $inputDataString .= $output;
        $inputDataString .= "\r\n#############################################";
        $inputDataString .= "\r\nERROR MESSAGE : ";
        
        $inputDataString .= $errorMessage;
        
        file_put_contents($filePath, $inputDataString);
        
        $emailLogRecipient = Mage::getStoreConfig('shipworks/general/email_log_recipient');
        if ($emailLogRecipient) {
            mail($emailLogRecipient, 'Shipworks Boostmyshop log ' . $filePath, $inputDataString);
        }
        
    }
    
}
