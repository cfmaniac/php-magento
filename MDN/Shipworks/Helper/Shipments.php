<?php

class MDN_Shipworks_Helper_Shipments extends Mage_Core_Helper_Abstract {

    const SENT_TO_SHIPWORKS = 1;
    const NOT_SENT_TO_SHIPWORKS = 0;

    public function getShipments($storeId)
    {
        $shipmentModelArray = array();

        $confData = Mage::getStoreConfig('shipworks/general/shipping_method');

        if ($confData != null && strlen($confData) > 0) {

            $allowedShippingMethod = explode(',', $confData);

            $collection = $this->getOrderPreparationItemsNotSentToShipworks();

            // Phill Zarfos - remove the SQL filter - we want to send ALL shipping methods to Shipworks
            //$collection->addFieldToFilter('shipping_method', array('in' => $allowedShippingMethod));

            foreach ($collection as $item) {
                $shipmentIncrementId = $item->getshipment_id();
                if (!empty($shipmentIncrementId)) {
                    try {
                        $shipmentModelInstance = Mage::getModel('sales/order_shipment')->load($shipmentIncrementId, 'increment_id');
                        if ($shipmentModelInstance != null && $shipmentModelInstance->getId() > 0) {
                            $shipmentModelArray[] = $shipmentModelInstance;
                        }
                    }catch(Exception $ex){
                        Mage::LogException($ex);
                    }
                }
            }
        }
 
        return $shipmentModelArray;
    }

    public function getOrderPreparationItemsNotSentToShipworks(){

        return Mage::getModel('Orderpreparation/ordertoprepare')
            ->getCollection()
            ->addFieldToFilter('sent_to_shipworks', self::NOT_SENT_TO_SHIPWORKS);
    }


    public function getShipmentsCount($storeId)
    {
        return count($this->getShipments($storeId));
    }

    public function flagAsSent($shipmentIncrementId)
    {
       $this->flagOrderPreparationItemByShipmentIncrementId($shipmentIncrementId,self::SENT_TO_SHIPWORKS);
    }
 
    public function flagAsNotSent($shipmentIncrementId)
    {
        $this->flagOrderPreparationItemByShipmentIncrementId($shipmentIncrementId,self::NOT_SENT_TO_SHIPWORKS);
    }

    private function flagOrderPreparationItemByShipmentIncrementId($shipmentIncrementId, $valueToFLag){

        $items = Mage::getModel('Orderpreparation/ordertoprepare')
            ->getCollection()
            ->addFieldToFilter('shipment_id', $shipmentIncrementId);
        foreach($items as $item)
        {
            $item->setsent_to_shipworks($valueToFLag)->save();
        }
    }

}
