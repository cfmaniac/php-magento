<?php

class MDN_Shipworks_Helper_Action_GetCount extends Mage_Core_Helper_Abstract {

    public function process() {
        // Write the params for easier diagnostics
        Mage::helper('Shipworks/Xml')->writeStartTag("Parameters");
        Mage::helper('Shipworks/Xml')->writeCloseTag("Parameters");
        Mage::helper('Shipworks/Xml')->writeElement("OrderCount", Mage::helper('Shipworks/Shipments')->getShipmentsCount(Mage::app()->getStore()->getStoreId()));
    }

}