<?php

class MDN_AdvancedStock_Adminhtml_AdvancedStock_BarcodeController extends Mage_Adminhtml_Controller_Action
{
	public function PrintLabelAction()
	{
        //
        // GetFPV - Phill Zarfos - Print UPC label
        // GetFPV - Phill Zarfos - use the SKU as the filename (instead of the generic "Barcode label.pdf")
        //

        //get param
        $productId = $this->getRequest()->getParam('product_id');
        $useUPC    = $this->getRequest()->getParam('useupc');
        $qty       = $this->getRequest()->getParam('qty');

        $obj = Mage::getModel('catalog/product')->load($productId);
        $sku = (is_object($obj)) ? trim($obj->getSku()) : 'barcode';

        //create pdf & download
        $obj = mage::getModel('AdvancedStock/Pdf_BarcodeLabels');
        $pdf = $obj->getPdf(array($productId => $qty), $useUPC);

        //this function prints the regular SKU label or the UPC label
        $basename = $sku;
        if ($useUPC) {
            $basename .= '_UPC';
        }

        $this->_prepareDownloadResponse($basename . '.pdf', $pdf->render(), 'application/pdf');
	}

    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('admin/erp/stock_management');
    }
}
