<?php

class MDN_AdvancedStock_Model_Convert_Adapter_ProductWarehouseStock extends Mage_Dataflow_Model_Convert_Adapter_Abstract {

    public function load() {
        // you have to create this method, enforced by Mage_Dataflow_Model_Convert_Adapter_Interface
    }

    public function save() {
        // you have to create this method, enforced by Mage_Dataflow_Model_Convert_Adapter_Interface
    }

    public function saveRow(array $importData) {

        //load data
        $sku = $importData['sku'];
        $qty = $importData['qty'];
        $warehouseCode = $importData['stock_code'];

        //check if product exists
        $productId = mage::getModel('catalog/product')->getIdBySku($sku);
        if (!$productId) {
            throw new Exception('Unable to load product with sku = ' . $sku);
        }

        //check if warehouse exists
        $warehouse = mage::getModel('AdvancedStock/Warehouse')->load($warehouseCode, 'stock_code');
        //
        // GetFPV - Phill Zarfos - fix the NULL 'stock_code' error
        // 
        // If you try to import warehouse stock data using Magento Admin > System > Import/Export > Advanced Data Profiles it will not work
        // The reason is that our 'stock_code' is NULL for all warehouses.
        // You can either research setting the 'stock_code' and going down that path,
        // or, load the warehouse model by loading it by ID as seen in the code below
        // You will also need to comment out the reference to $importData['stock_code'] above
        // 
        // However, it takes more than 1 hour to import stock values this way, and will
        // send hundreds of emails for all the stock movements.
        // so that is why I wrote the shell/getfpv_inventory_import.php script which is MUCH faster.
        //
        //$warehouseID = 1;  // this is the warehouse named 'Default' 
        //$warehouse = mage::getModel('AdvancedStock/Warehouse')->load($warehouseID, 'stock_id');        
        if (!$warehouse || !$warehouse->getId()) {
            throw new Exception('Unable to load warehouse with code ' . $warehouseCode);
        }

        //update stock
        $stockItem = $this->getStockItem($productId, $warehouse);
        foreach($importData as $key => $value)
        {
            $stockItem->setData($key, $value);
        }
        $stockItem->save();

        //if is favorite, ensure that other are not favorite
        $isFavorite = (isset($importData['is_favorite_warehouse']) ? $importData['is_favorite_warehouse'] : 0);
        if ($isFavorite)
        {
            $stocks = Mage::helper('AdvancedStock/Product_Base')->getStocks($productId);
            foreach($stocks as $otherStock)
            {
                if ($otherStock->getId() == $stockItem->getId())
                        continue;
                if ($otherStock->getis_favorite_warehouse())
                        $otherStock->setis_favorite_warehouse(0)->save();
            }
        }

        return true;
    }

    public function getStockItem($productId, $warehouse)
    {
        $stockItem = $warehouse->getProductStockItem($productId);

        //if stock doesn't exist, create it
        if (!$stockItem || !$stockItem->getId())
            $stockItem = mage::getModel('cataloginventory/stock_item')->createStock($productId, $warehouse->getId());

        return $stockItem;
    }

}