<?php
/**
 * Orbitvu PHP eCommerce Orbitvu DB drivers
 * @Copyright: Orbitvu Sp. z o.o. is the owner of full rights to this code
 *
 * @For: Magento
 */

/*
 * Start installation
 */

$installer = $this;
$installer->startSetup();

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/mousewheel', '1')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('mousewheel', 'true', 'mode', '')");

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/gui_background', '#000000')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('gui_background', '#000000', 'mode', '')");

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/gui_foreground', '#ffffff')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('gui_foreground', '#ffffff', 'mode', '')");

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/gui_foreground_active', '#21B6A8')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('gui_foreground_active', '#21B6A8', 'mode', '')");

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/append_prepend', 'append')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('append_prepend', 'append', 'mode', '')");

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/gui_style', '0')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('gui_style', '0', 'mode', '')");

$installer->endSetup();
?>