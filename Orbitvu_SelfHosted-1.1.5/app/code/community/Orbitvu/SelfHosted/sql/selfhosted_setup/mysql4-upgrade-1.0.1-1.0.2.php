<?php
/**
 * Orbitvu PHP eCommerce Orbitvu DB drivers
 * @Copyright: Orbitvu Sp. z o.o. is the owner of full rights to this code
 *
 * @For: Magento
 */

/*
 * Start installation
 */

$installer = $this;
$installer->startSetup();

$installer->run("INSERT INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/layout/scroll', 'yes')");
$installer->run("INSERT INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/layout/img_tn_padding', '2px')");

$installer->endSetup();
?>