<?php
/**
 * Orbitvu PHP eCommerce Orbitvu DB drivers
 * @Copyright: Orbitvu Sp. z o.o. is the owner of full rights to this code
 *
 * @For: Magento
 */

/*
 * Start installation
 */

$installer = $this;
$installer->startSetup();

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/preload_width', '0')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('preload_width', '0', 'mode', '')");

$installer->run("INSERT IGNORE INTO {$this->getTable('core_config_data')} (`path`, `value`) VALUES ('orbitvu_selfhosted/mode/preload_height', '0')");
$installer->run("INSERT IGNORE INTO {$this->getTable('orbitvu_sh_configuration')} (`var`, `value`, `type`, `info`) VALUES ('preload_height', '0', 'mode', '')");

$installer->endSetup();
?>