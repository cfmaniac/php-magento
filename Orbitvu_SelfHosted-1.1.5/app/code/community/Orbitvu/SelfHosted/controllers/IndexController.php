<?php
/**
 * @category    Orbitvu
 * @package     Orbitvu_SelfHosted
 * @copyright   Copyright (C) 2014 Orbitvu (http://www.orbitvu.com)
 * @license     http://www.orbitvu.com/plugins/license
 * @version     1.0.0
 */

class Orbitvu_SelfHosted_IndexController extends Mage_Core_Controller_Front_Action {

    /**
     * Orbitvu remote upload actions switcher
     * @return string XML response
     */
    public function indexAction() {
        //----------------------------------------------------------------------
        $observer = Mage::getSingleton('selfhosted/observer');
        $observer->ExtendOrbitvu();
        $observer->_Orbitvu->StartRemoteListener();
        //----------------------------------------------------------------------
    }
    
}

?>