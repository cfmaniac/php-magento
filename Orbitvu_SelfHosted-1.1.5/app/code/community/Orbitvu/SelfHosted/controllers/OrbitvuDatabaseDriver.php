<?php
/**
 * Orbitvu PHP eCommerce Orbitvu DB drivers
 * @Copyright: Orbitvu Sp. z o.o. is the owner of full rights to this code
 *
 * @for: Magento
 *
 * This is the only class to modify for your store
 */

class OrbitvuDatabaseDriver {

    /**
     * Database instance
     * @var instance
     */
    private $db_handle;

    /**
     * Database last insert ID
     * @var integer
     */
    private $db_last_id = 0;

    /**
     * Driver version (ID)
     */
    const DRIVER_VERSION = '1.1.1';

    /**
     * Connect to database
     */
    public function __construct() {
        //---------------------------------------------------------------------------------------------------
        $this->db_handle = Mage::getSingleton('core/resource')->getConnection('core_write');
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Get database prefix
     * @return string
     */
    public function GetPrefix() {
        //---------------------------------------------------------------------------------------------------
        return Mage::getConfig()->getTablePrefix(); // Store tables prefix
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Make a SQL query
     * @param string $db_query SQL query
     * @return boolean|Exception
     */
    public function Query($db_query) {
        //---------------------------------------------------------------------------------------------------
        return $this->db_handle->query($db_query); // Store query function
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Make a SQL query and return fetched items array
     * @param string $db_query SQL query
     * @return array|Exception
     */
    public function FetchAll($db_query) {
        //---------------------------------------------------------------------------------------------------
        return $this->db_handle->fetchAll($db_query); // Store query & fetch_array function + list results as array
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Escape SQL query string
     * @param string $query_string SQL query
     * @return type
     */
    public function Escape($query_string) {
        //---------------------------------------------------------------------------------------------------
        //return mysql_escape_string(str_replace("'", "\'", $query_string));
        $query_string = Mage::getSingleton('core/resource')->getConnection('default_write')->quote(str_replace("'", "\'", $query_string));
        $query_string = substr($query_string, 1, count($query_string) - 2);
        return $query_string;
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Return store last insert ID
     * @return integer
     */
    private function last_insert_id() {
        //---------------------------------------------------------------------------------------------------
        return $this->db_handle->lastInsertId(); // Store SQL last_insert_id
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * SQL last insert ID
     * @return integer
     */
    public function GetLastInsertId() {
        //---------------------------------------------------------------------------------------------------
        $last_id = intval($this->last_insert_id()); // Store SQL last_insert_id
        //---------------------------------------------------------------------------------------------------
        if ($last_id > 0 && $last_id != $this->db_last_id) {
            $this->db_last_id = $last_id;
        }
        //---------------------------------------------------------------------------------------------------
        return $this->db_last_id;
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Get store configuration
     * @return array
     */
    public function GetConfiguration() {
        //---------------------------------------------------------------------------------------------------
        $config = array(
            'local_session_key'     => Mage::getStoreConfig('orbitvu_selfhosted/api/local_session_key'),
            'sync_2d'               => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/items/sync_2d')),
            'sync_360'              => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/items/sync_360')),
            'auto_sync'             => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/synchro/auto_sync')),
            'width'                 => Mage::getStoreConfig('orbitvu_selfhosted/layout/width'),
            'height'                => Mage::getStoreConfig('orbitvu_selfhosted/layout/height'),
            'border_color'          => Mage::getStoreConfig('orbitvu_selfhosted/layout/border_color'),
            'img_width'             => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_width'),
            'img_height'            => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_height'),
            'img_width_zoom'        => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_width_zoom'),
            'img_height_zoom'       => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_height_zoom'),
            'scroll'                => Mage::getStoreConfig('orbitvu_selfhosted/layout/scroll'),
            'img_width_tn'          => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_width_tn'),
            'img_height_tn'         => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_height_tn'),
            'img_tn_margin'         => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_tn_margin'),
            'img_tn_padding'        => Mage::getStoreConfig('orbitvu_selfhosted/layout/img_tn_padding'),
            'button_width'          => Mage::getStoreConfig('orbitvu_selfhosted/layout/button_width'),
            'button_height'         => Mage::getStoreConfig('orbitvu_selfhosted/layout/button_height'),
            'button_opacity'        => Mage::getStoreConfig('orbitvu_selfhosted/layout/button_opacity'),
            'hover_mode'            => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/mode/hover_mode')),
            'hover_delay'           => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/mode/hover_delay')),
            'mousewheel'            => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/mode/mousewheel')),
            'gui_background'        => $this->make_color_for_viewer(Mage::getStoreConfig('orbitvu_selfhosted/mode/gui_background')),
            'gui_foreground'        => $this->make_color_for_viewer(Mage::getStoreConfig('orbitvu_selfhosted/mode/gui_foreground')),
            'gui_foreground_active' => $this->make_color_for_viewer(Mage::getStoreConfig('orbitvu_selfhosted/mode/gui_foreground_active')),
            'gui_style'             => Mage::getStoreConfig('orbitvu_selfhosted/mode/gui_style'),
            'teaser'                => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/mode/teaser')),
            'html5'                 => $this->make_boolean(Mage::getStoreConfig('orbitvu_selfhosted/mode/html5')),
            'append_prepend'        => Mage::getStoreConfig('orbitvu_selfhosted/mode/append_prepend'),
            'preload_width'         => Mage::getStoreConfig('orbitvu_selfhosted/mode/preload_width'),
            'preload_height'        => Mage::getStoreConfig('orbitvu_selfhosted/mode/preload_height'),
            'viewers_path'          => Mage::getBaseDir().DIRECTORY_SEPARATOR.Mage::getStoreConfig('orbitvu_selfhosted/main/viewers_path').DIRECTORY_SEPARATOR,
            'presentations_path'    => Mage::getBaseDir().DIRECTORY_SEPARATOR.Mage::getStoreConfig('orbitvu_selfhosted/main/presentations_path').DIRECTORY_SEPARATOR,
            'viewers_path_relative' => Mage::getStoreConfig('orbitvu_selfhosted/main/viewers_path').'/',
            'presentations_path_relative' => Mage::getStoreConfig('orbitvu_selfhosted/main/presentations_path').'/',
            'viewer'                => Mage::getStoreConfig('orbitvu_selfhosted/main/viewer'),
            'viewer_previous'       => Mage::getStoreConfig('orbitvu_selfhosted/main/viewer_previous')
        );
        //---------------------------------------------------------------------------------------------------
        return $config;
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Set store configuration.
     * Do not append local plugin configuration directly
     * @param string $var Var from configuration
     * @param string $value New vakue
     * @return boolean
     */
    public function SetConfiguration($var, $value) {
        //---------------------------------------------------------------------------------------------------
        $config = new Mage_Core_Model_Config();
        $query = $this->FetchAll('SELECT `type` FROM `'.$this->GetPrefix().'orbitvu_sh_configuration` WHERE `var` = \''.$var.'\' LIMIT 1');
        $type = $query[0]['type'];
        $config->saveConfig('orbitvu_selfhosted/'.$type.'/'.$var, $value, 'default', 0);
        Mage::app()->getCacheInstance()->cleanType('config');
        //---------------------------------------------------------------------------------------------------
        return true;
        //---------------------------------------------------------------------------------------------------
    }


    /**
     * Get store plugin version
     * @return string
     */
    public function GetVersion() {
        //---------------------------------------------------------------------------------------------------
        $module = $this->get_module_name();

        return (string) Mage::getConfig()->getNode()->modules->$module->version;
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Get local session key
     * @return string
     */
    public function GetLocalSessionKey() {
        $config = $this->GetConfiguration();

        //---------------------------------------------------------------------------------------------------
        if (!$config['local_session_key']) {
            $key = md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH']));
            $this->SetConfiguration('local_session_key', $key);
            return $key;
        }
        return $config['local_session_key'];
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Get remote authorization URL
     * @return string
     */
    public function GetRemoteAuthorizationUrl() {
        if (Mage::getStoreConfig('web/url/use_store')) {
            $default_store_id = Mage::app()
                ->getWebsite(1)
                ->getDefaultGroup()
                ->getDefaultStoreId();
            //---------------------------------------------------------------------------------------------------
            return Mage::getUrl('ovremote', array('_store' => $default_store_id)).'?ov_action=auth&ov_key='.$this->GetLocalSessionKey();
            //---------------------------------------------------------------------------------------------------
        }
        //---------------------------------------------------------------------------------------------------
        return Mage::getUrl('ovremote').'?ov_action=auth&ov_key='.$this->GetLocalSessionKey();
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Gets remote upload URL
     * @return string
     */
    public function GetRemoteUploadUrl() {
        if (Mage::getStoreConfig('web/url/use_store')) {
            $default_store_id = Mage::app()
                ->getWebsite(1)
                ->getDefaultGroup()
                ->getDefaultStoreId();
            //---------------------------------------------------------------------------------------------------
            return Mage::getUrl('ovremote', array('_store' => $default_store_id)).'?ov_action=upload&ov_key='.$this->GetLocalSessionKey();
            //---------------------------------------------------------------------------------------------------
        }
        //---------------------------------------------------------------------------------------------------
        return Mage::getUrl('ovremote').'?ov_action=upload&ov_key='.$this->GetLocalSessionKey();
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Gets administration URL
     * @return string
     */
    public function GetAdminUrl() {
        //---------------------------------------------------------------------------------------------------
        return Mage::getBaseUrl().'admin/';
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Gets product URL
     * @return string
     */
    public function GetProductUrl($product_id) {
        //---------------------------------------------------------------------------------------------------
        return Mage::getModel('catalog/product')->load($product_id)->getUrlInStore();
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Get all products with ORM and prepare to synchronize
     * @return array
     */
    public function SynchronizeAllProducts() {
        //-------------------------------------------------------------------------------------------------------
        $storeId    = Mage::app()->getStore()->getId();
        $product    = Mage::getModel('catalog/product');
        //-------------------------------------------------------------------------------------------------------
        $products = $product->getCollection()
            ->addAttributeToSelect(array(
                'name',
                'sku'
            ))
            ->addAttributeToFilter('status', '1');
        //-------------------------------------------------------------------------------------------------------
        $products_array = array();
        foreach ($products as $q) {
            $products_array[] = array(
                'product_id'	=> $q->getId(),
                'product_name'	=> $q->getName(),
                'product_sku'	=> $q->getSku()
            );
        }
        //-------------------------------------------------------------------------------------------------------
        return $products_array;
        //-------------------------------------------------------------------------------------------------------   
    }

    /**
     * Install database tables
     * @return boolean
     */
    public function Install() {
        //-------------------------------------------------------------------------------------------------------
        $prefix = $this->GetPrefix();
        //---------------------------------------------------------------------------------------------------
        $dump = "
    
            INSERT IGNORE INTO `".$prefix."core_config_data` 
                (`scope`, `scope_id`, `path`, `value`) 
            VALUES
                ('default', 0, 'orbitvu_selfhosted/api/local_session_key', '".md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH']))."'),
                ('default', 0, 'orbitvu_selfhosted/mode/html5', 'yes'),
                ('default', 0, 'orbitvu_selfhosted/mode/append_prepend', 'append'),
                ('default', 0, 'orbitvu_selfhosted/mode/hover_delay', '2'),
                ('default', 0, 'orbitvu_selfhosted/mode/mousewheel', '1'),
                ('default', 0, 'orbitvu_selfhosted/mode/gui_background', '#000000'),
                ('default', 0, 'orbitvu_selfhosted/mode/gui_foreground', '#ffffff'),
                ('default', 0, 'orbitvu_selfhosted/mode/gui_foreground_active', '#21B6A8'),
                ('default', 0, 'orbitvu_selfhosted/mode/gui_style', '0'),
                ('default', 0, 'orbitvu_selfhosted/mode/teaser', 'autorotate'),
                ('default', 0, 'orbitvu_selfhosted/mode/hover_mode', '1'),
                ('default', 0, 'orbitvu_selfhosted/mode/preload_width', '0'),
                ('default', 0, 'orbitvu_selfhosted/mode/preload_height', '0'),
                ('default', 0, 'orbitvu_selfhosted/layout/button_opacity', '1'),
                ('default', 0, 'orbitvu_selfhosted/layout/button_height', '53px'),
                ('default', 0, 'orbitvu_selfhosted/layout/button_width', '30px'),
                ('default', 0, 'orbitvu_selfhosted/layout/scroll', 'yes'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_tn_margin', '3px'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_tn_padding', '2px'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_height_tn', '50px'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_width_tn', '75px'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_height_zoom', '768px'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_width_zoom', '1024px'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_height', '300px'),
                ('default', 0, 'orbitvu_selfhosted/layout/img_width', '583px'),
                ('default', 0, 'orbitvu_selfhosted/layout/border_color', '#cccccc'),
                ('default', 0, 'orbitvu_selfhosted/layout/height', '361px'),
                ('default', 0, 'orbitvu_selfhosted/layout/width', '100%'),
                ('default', 0, 'orbitvu_selfhosted/advanced/language', 'en'),
                ('default', 0, 'orbitvu_selfhosted/items/sync_360', '1'),
                ('default', 0, 'orbitvu_selfhosted/items/sync_2d', '1'),
                ('default', 0, 'orbitvu_selfhosted/main/viewers_path', '_orbitvu_viewers'),
                ('default', 0, 'orbitvu_selfhosted/main/presentations_path', '_orbitvu_presentations'),
                ('default', 0, 'orbitvu_selfhosted/synchro/auto_sync', '0'),
                ('default', 0, 'orbitvu_selfhosted/main/viewer', 'BASIC');
       
            CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_configuration` (
                `id` int(3) NOT NULL auto_increment,
                  `priority` int(2) NOT NULL,
                  `var` varchar(30) NOT NULL,
                  `value` text NOT NULL,
                  `type` varchar(20) NOT NULL,
                  `info` varchar(200) NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `var` (`var`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

            INSERT IGNORE INTO `".$prefix."orbitvu_sh_configuration` 
                (`var`, `value`, `type`, `info`) 
            VALUES
                ('local_session_key', '".md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH']))."', 'api', ''),
                ('viewers_path', '_orbitvu_viewers', 'main', 'Your viewers upload folder (chmod 777)'),
                ('presentations_path', '_orbitvu_presentations', 'main', 'Your presentations upload folder (chmod 777)'),
                ('viewers_path_relative', '_orbitvu_viewers', 'main', 'Your viewers upload folder (chmod 777)'),
                ('presentations_path_relative', '_orbitvu_presentations', 'main', 'Your presentations upload folder (chmod 777)'),
                ('temp_path', 'tmp/', 'main', 'Your server''s temporary path (chmod 777)'),
                ('using_sun', 'true', 'main', 'Are you using Orbitvu SUN Cloud?'),
                ('last_updated', '2014-07-16 10:29:46', 'main', 'Last Orbitvu SUN Cloud synchronization date'),
                ('last_refreshed', '2014-07-16 10:29:46', 'main', 'Last update date'),
                ('auto_sync', 'false', 'synchro', 'Synchronize presentations automatically?'),
                ('sync_2d', 'true', 'items', 'If 2D photos exists - synchronize them?'),
                ('sync_360', 'true', 'items', 'If 360 exists - synchronize it?'),
                ('hover_mode', 'true', 'mode', ''),
                ('width', '100%', 'layout', ''),
                ('height', '361px', 'layout', ''),
                ('border_color', '#cccccc', 'layout', ''),
                ('img_width', '583px', 'layout', ''),
                ('img_height', '300px', 'layout', ''),
                ('img_width_zoom', '1024px', 'layout', ''),
                ('img_height_zoom', '768px', 'layout', ''),
                ('scroll', 'yes', 'layout', ''),
                ('img_width_tn', '75px', 'layout', ''),
                ('img_height_tn', '50px', 'layout', ''),
                ('img_tn_margin', '3px', 'layout', ''),
                ('img_tn_padding', '2px', 'layout', ''),
                ('button_width', '30px', 'layout', ''),
                ('button_height', '53px', 'layout', ''),
                ('button_opacity', '1', 'layout', ''),
                ('mousewheel', 'true', 'mode', ''),
                ('gui_background', '#000000', 'mode', ''),
                ('gui_foreground', '#ffffff', 'mode', ''),
                ('gui_foreground_active', '#21B6A8', 'mode', ''),
                ('gui_style', '0', 'mode', ''),
                ('teaser', 'autorotate', 'mode', ''),
                ('hover_delay', '2', 'mode', ''),
                ('html5', 'yes', 'mode', ''),
                ('append_prepend', 'append', 'mode', ''),
                ('preload_width', '0', 'mode', ''),
                ('preload_height', '0', 'mode', ''),
                ('first_time', 'true', 'main', ''),
                ('last_flushed_cache', '2014-07-16 10:29:46', 'main', 'Last Orbitvu cache flushed'),
                ('viewer', 'BASIC', 'main', 'Installed Orbitvu Viewer'),
                ('viewer_previous', 'BASIC', 'main', 'Installed Orbitvu Viewer');
                
            CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_log` (
                `id` int(11) NOT NULL auto_increment,
                  `_item_id` int(11) NOT NULL,
                  `_item_table` enum('_presentations','_presentations_items','_viewers') NOT NULL,
                  `action` enum('add','delete','info','skip','update') NOT NULL DEFAULT 'info',
                  `comment` varchar(150) NOT NULL,
                  `date` datetime NOT NULL,
                  `ip` varchar(20) NOT NULL,
                  PRIMARY KEY (`id`), 
                  KEY `_item_id` (`_item_id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

            CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_products_presentations` (
                `id` int(11) NOT NULL auto_increment,
                  `product_id` int(11) NOT NULL,
                  `orbitvu_id` int(11) NOT NULL,
                  `name` varchar(150) NOT NULL,
                  `config` text NOT NULL,
                  `viewer` int(2) NOT NULL,
                  `type` enum('sun','local') NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `product_id` (`product_id`), 
                  KEY `orbitvu_id` (`orbitvu_id`)
                ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
                

            CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_products_presentations_cache` (
                `id` int(11) NOT NULL auto_increment,
                  `dir` varchar(255) NOT NULL,
                  `name` varchar(150) NOT NULL,
                  `date` date DEFAULT NULL,
                  `content` text NOT NULL,
                  PRIMARY KEY (`id`),
                  UNIQUE KEY `dir` (`dir`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

            CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_products_presentations_history` (
                `id` int(11) NOT NULL auto_increment,
                  `product_id` int(11) NOT NULL,
                  `orbitvu_id` int(11) NOT NULL,
                  `unlink_date` datetime NOT NULL,
                  PRIMARY KEY (`id`), 
                  KEY `product_id` (`product_id`,`orbitvu_id`), 
                  KEY `orbitvu_id` (`orbitvu_id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

            CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_products_presentations_items` (
                `id` int(11) NOT NULL auto_increment,
                  `_presentations_id` int(11) NOT NULL,
                  `orbitvu_id` varchar(32) NOT NULL,
                  `priority` int(3) NOT NULL,
                  `name` varchar(150) NOT NULL,
                  `type` tinyint(1) NOT NULL,
                  `thumbnail` varchar(200) NOT NULL,
                  `path` varchar(200) NOT NULL,
                  `config` text NOT NULL,
                  `status` enum('active','inactive') NOT NULL DEFAULT 'active',
                  PRIMARY KEY (`id`), 
                  KEY `presentation_id` (`_presentations_id`,`orbitvu_id`)
                ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

            CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_products_thumbnails` (
                `product_id` int(11) NOT NULL,
                `thumbnail` varchar(200) NOT NULL,
                PRIMARY KEY (`product_id`)
              ) ENGINE=InnoDB DEFAULT CHARSET=utf8;

	    CREATE TABLE IF NOT EXISTS `".$prefix."orbitvu_sh_viewers` (
                `id` int(3) NOT NULL auto_increment,
                `name` varchar(50) NOT NULL,
                `version` varchar(10) NOT NULL,
                `support` date NOT NULL,
                `license` varchar(100) NOT NULL,
                `date_install` datetime NOT NULL,
                `path` varchar(100) NOT NULL,
                PRIMARY KEY(`id`)
              ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ";

        $dump = explode(';', $dump);

        foreach ($dump as $query) {
            try {
                $this->Query($query);
            }
            catch (Exception $e) {}
        }
        //---------------------------------------------------------------------------------------------------
        return true;
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Gets module name
     * @return string
     */
    private function get_module_name() {
        //---------------------------------------------------------------------------------------------------
        $v = self::DRIVER_VERSION;
        $suf = 'Orbitvu_';

        if ($v{2} == '1') {
            $suf .= 'SelfHosted';
        }
        else {
            $suf .= 'Sun';
        }

        return $suf;
        //---------------------------------------------------------------------------------------------------
    }

    /**
     * Make boolean from integers
     * @param integer $value
     * @return boolean|string
     */
    private function make_boolean($value) {
        //---------------------------------------------------------------------------------------------------
        return str_replace(array('1', '0'), array('true', 'false'), $value);
        //---------------------------------------------------------------------------------------------------
    }

    private function make_color_for_viewer($color) {
        $color = ltrim($color, "#");
        return '0x'.$color;
    }

}
?>