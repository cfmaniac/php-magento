<?php
/**
 * @category    Orbitvu
 * @package     Orbitvu_SelfHosted
 * @copyright   Copyright (C) 2014 Orbitvu (http://www.orbitvu.com)
 * @license     http://www.orbitvu.com/plugins/license
 * @version     1.0.0
 */

class Orbitvu_SelfHosted_Model_Observer {
    // Mark as Singleton (for Magento)
    static protected $_singletonFlag = false;
    
    public $_Orbitvu;
    //-------------------------------------------------------------------------------------------------------
    public function ExtendOrbitvu($return = true) {
        $no = strpos(dirname((__FILE__)), "Orbitvu".DIRECTORY_SEPARATOR."SelfHosted");
        $dir = substr(dirname(__FILE__), 0, $no ? $no : strlen(dirname(__FILE__)));
        $helpers = $dir.DIRECTORY_SEPARATOR."Orbitvu".DIRECTORY_SEPARATOR."SelfHosted".DIRECTORY_SEPARATOR."controllers".DIRECTORY_SEPARATOR."OrbitvuAdmin.php";
        include_once($helpers);

        /*$helpers = __DIR__.'/../controllers/OrbitvuAdmin.php';
    	include_once($helpers);*/
    	
    	$this->_Orbitvu = new OrbitvuAdmin(false);
    	//-------------------------------------------------------------------------------------------------------
    	if ($return) {
            return $this->_Orbitvu;
    	}
    	//-------------------------------------------------------------------------------------------------------
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Synchronize one thumbnail
     */
    public function UpdateThumbnails($product_id) {
        //-------------------------------------------------------------------------------------------------------
        $this->ExtendOrbitvu();
        //-------------------------------------------------------------------------------------------------------
        $CurrentProduct = Mage::getModel('catalog/product')->load($product_id);
        $ProductHelper = Mage::helper('catalog/product');
        $medias = $CurrentProduct->getMediaGalleryImages();
        $orbithumbs = $this->_Orbitvu->GetProductThumbnails($product_id);
        //-------------------------------------------------------------------------------------------------------
        $o_thumbs_count = intval(count($orbithumbs)); // = 1 or 0
        $m_thumbs_count = intval(count($medias));
        $n = $m_thumbs_count - $o_thumbs_count;
        //-------------------------------------------------------------------------------------------------------
        //if ($o_thumbs_count > 0) {
            //$this->DeleteThumbnail($product_id, $orbithumbs[0]);
            //$o_thumbs_count = 0;
        //}
        //-------------------------------------------------------------------------------------------------------
        if ($m_thumbs_count > 1 && $o_thumbs_count == 0) {
            // magento own images - do nothing
        }
        else if ($o_thumbs_count == 0 && $m_thumbs_count == 0) {
            //-------------------------------------------------------------------------------------------------------
            try {
                $new_thumbnail = $this->AddThumbnail($product_id);
                if (!empty($new_thumbnail)) {
                    $this->_Orbitvu->SetProductThumbnail($product_id, $new_thumbnail);
                }
            }
            catch (Exception $e) {}
            //-------------------------------------------------------------------------------------------------------
        }
        //-------------------------------------------------------------------------------------------------------
        return true;
        //-------------------------------------------------------------------------------------------------------
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Add thumbnail
     */
    public function AddThumbnail($product_id, $configurable_id = false) {
        //-------------------------------------------------------------------------------------------------------
        $presentation = $this->_Orbitvu->GetProductPresentation($configurable_id ? $configurable_id : $product_id, true);
        //-------------------------------------------------------------------------------------------------------
        if (count($presentation['items']) <= 0) {
            return false;
        }
        //-------------------------------------------------------------------------------------------------------
        $file = $this->GetThumbnailPath($presentation['items'][0]['thumbnail']);
        
        if (stristr($file, '.png')) {
            $ext = 'png';
        }
        else {
            $ext = 'jpeg';
        }
        //-------------------------------------------------------------------------------------------------------
        $sun = file_get_contents($file);
        //-------------------------------------------------------------------------------------------------------
        $media_api = Mage::getModel('catalog/product_attribute_media_api');
        $pmodel = Mage::getModel('catalog/product');
        $_product = $pmodel->load($product_id);
        //-------------------------------------------------------------------------------------------------------
        $newImage = array(
            'file' => array(
                'name'      => md5(date('H:i:s Y-m-d').'Orbitvu Thumbnail'.mt_rand(0, 999999)),
                'content'   => base64_encode($sun),
                'mime'      => 'image/'.$ext
            ),
            'label'    => $_product->getName(),
            'position' => 1,
            'types'    => array('thumbnail', 'small_image', 'image'),
            'exclude'  => 1
        );
        //-------------------------------------------------------------------------------------------------------
        return $media_api->create($product_id, $newImage);
        //-------------------------------------------------------------------------------------------------------
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Delete thumbnail
     */
    public function DeleteThumbnail($product_id, $orbitvu_entry) {
        //-------------------------------------------------------------------------------------------------------
        $mediaApi = Mage::getModel('catalog/product_attribute_media_api');
        $items = $mediaApi->items($product_id);
        $this->_Orbitvu->DeleteProductThumbnail($product_id);
        //-------------------------------------------------------------------------------------------------------
        foreach ($items as $item) {
            if ($item['file'] == $orbitvu_entry['thumbnail']) {
                $mediaApi->remove($product_id, $item['file']);
                $fileName = Mage::getBaseDir('media').'/catalog/product'.$item['file'];

                if (file_exists($fileName)) {
                    unlink($fileName);
                }
            }
        }
        //-------------------------------------------------------------------------------------------------------
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Synchronize thumbs
     */
    public function SynchronizeAllThumbnails() {
        //------------------------------------------------------------------------------------------------------- 
        $storeId    = Mage::app()->getStore()->getId();  
        $product    = Mage::getModel('catalog/product');
        //-------------------------------------------------------------------------------------------------------
        $products = $product->getCollection()
                            ->addAttributeToSelect(array(
                                'name',
                                'sku'
                            ))
                            ->addAttributeToFilter('status', '1');
        //-------------------------------------------------------------------------------------------------------
        $products_array = array();
        foreach ($products as $q) {
            $this->UpdateThumbnails($q->getId());
        }
        //------------------------------------------------------------------------------------------------------- 
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Get Thumbnail Path
     */
    public function GetThumbnailPath($thumbnail_url, $width = null, $height = null) {
        //------------------------------------------------------------------------------------------------------- 
        $thumbnail = Mage::getBaseDir().'/'.$thumbnail_url;

        if (!file_exists($thumbnail) || empty($thumbnail_url)) {
            $thumbnail = Mage::getBaseUrl('media').'orbitvu/placeholder.png';
        }
        else {
            /**
             * Resize image if needed
             */
            //------------------------------------------------------------------------------------------------------- 
            if ($width !== null || $height !== null) {
                $thumbnail = $this->_Orbitvu->ResizeImage($thumbnail, $width, $height);
                $thumbnail_url = str_replace(array(Mage::getBaseDir().'/', Mage::getBaseDir(), '//'), array('', '', '/'), $thumbnail);
            }
            //------------------------------------------------------------------------------------------------------- 
            $thumbnail = str_replace('index.php/', '', Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB)).$thumbnail_url;
            //------------------------------------------------------------------------------------------------------- 
        }
        $thumbnail = str_replace('\\', "/", $thumbnail);
        //------------------------------------------------------------------------------------------------------- 
        return $thumbnail;
        //------------------------------------------------------------------------------------------------------- 
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Make synchronization
     */
    public function SynchronizeAllProducts() {
        //------------------------------------------------------------------------------------------------------- 
        $products = $this->_Orbitvu->SynchronizeAllProducts();
        foreach ($products as $product) {
            $this->UpdateThumbnails($product['product_id']);
        }
        //------------------------------------------------------------------------------------------------------- 
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Simple redirect
     */
    public function RedirectAlias($url) {
        //------------------------------------------------------------------------------------------------------- 
        $srv = Mage::getUrl($url);
        return Mage::app()->getResponse()->setHeader('Location', $srv)->sendHeaders();  
        //------------------------------------------------------------------------------------------------------- 
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Own templates
     */
    public function UseTemplate($ar, $template) {
        //------------------------------------------------------------------------------------------------------- 
        return $this->_Orbitvu->UseTemplate($ar, $template);
        //------------------------------------------------------------------------------------------------------- 
    }
    //------------------------------------------------------------------------------------------------------- 
    /*
     * Save product
     */
    public function saveProductTabData(Varien_Event_Observer $observer) {
    	//------------------------------------------------------------------------------------------------------- 
        if (!self::$_singletonFlag) {
            self::$_singletonFlag = true;

            //-------------------------------------------------------------------------------------------------------
            $this->ExtendOrbitvu();
            //-------------------------------------------------------------------------------------------------------
            
            /*
             * Upload new presentations
             */
            if (!empty($_FILES['ovus_upload']['tmp_name'])) {
                $archive_name = $this->_Orbitvu->GetPresentationsPath().$_FILES['ovus_upload']['name'];
                copy($_FILES['ovus_upload']['tmp_name'], $archive_name);
                $this->_Orbitvu->InstallPresentation($archive_name, false);
            }
            else if (isset($_FILES['ovus_upload']['error']) && ($_FILES['ovus_upload']['error'] == '1')) {
                $cookie = Mage::getSingleton('core/cookie');
                $cookie->set('error_max_filesize', 'true', time()+3600*24*7);
            }
            
            /*
             * Update Orbitvu thumbnails
             */
            //-------------------------------------------------------------------------------------------------------
            if (Mage::app()->getRequest()->getParam('selfhosted') != 'update') {
                if (is_object(Mage::registry('product'))) {
                    $product_id = Mage::registry('product')->getId();
                    $this->UpdateThumbnails($product_id);

                    /*
                     * (No) Assign images to associated products
                     */
                    if (Mage::registry('product')->isConfigurable()) {
                        $should_assign = (bool)Mage::app()->getRequest()->getParam('orbitvu_assign_configurable');
                        if ($should_assign) {
                            $associated = $this->_Orbitvu->GetAssociatedProducts(Mage::registry('product')->getId());
                            foreach($associated as $product) {
                                $pr = Mage::getModel('catalog/product')->load((int)$product['product_id']);
                                $pr->load('media_gallery');
                                $medias = $pr->getMediaGallery();
                                //-------------------------------------------------------------------------------------------------------
                                $m_thumbs_count = intval(count($medias['images']));
                                if ($m_thumbs_count <= 0) {
                                    $this->AddThumbnail($product['product_id'], Mage::registry('product')->getId());
                                }
                            }
                        }
                    }
                }
            }
            //-------------------------------------------------------------------------------------------------------
        }
        //------------------------------------------------------------------------------------------------------- 
    }
}

?>