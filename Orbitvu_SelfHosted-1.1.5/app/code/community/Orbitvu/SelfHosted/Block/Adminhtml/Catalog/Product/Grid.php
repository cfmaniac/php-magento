<?php
/**
 * @category    Orbitvu
 * @package     Orbitvu_SelfHosted
 * @copyright   Copyright (C) 2014 Orbitvu (http://www.orbitvu.com)
 * @license     http://www.orbitvu.com/plugins/license
 * @version     1.0.0
 */

class Orbitvu_SelfHosted_Block_Adminhtml_Catalog_Product_Grid extends Mage_Adminhtml_Block_Catalog_Product_Grid {

    const XML_SEVERITY_ICONS_URL_PATH  = 'system/adminnotification/severity_icons_url';

    /*
     * Get column "Orbitvu"
     * for products view
     */
    public function setCollection($collection) {
        //------------------------------------------------------------------------------------------------------------------
        //$store = $this->_getStore();
        //------------------------------------------------------------------------------------------------------------------
        /*if ($store->getId() && !isset($this->_joinAttributes['orbitvu_selfhosted'])) {
            $collection->joinAttribute(
                'orbitvu_selfhosted', 'catalog_product/orbitvu_selfhosted', 'entity_id', null, 'left', $store->getId()
            );
        }
        else {
            $collection->addAttributeToSelect('orbitvu_selfhosted');
        }*/
        //------------------------------------------------------------------------------------------------------------------
        parent::setCollection($collection);
        //------------------------------------------------------------------------------------------------------------------
    }

    /*
     * Don't know why Magento links this notification icons to external resources
     * But OK...
     */
    public function getSeverityIconsUrl($icon_type = 'NOTICE') {
        //------------------------------------------------------------------------------------------------------------------
        $url = (Mage::app()->getFrontController()->getRequest()->isSecure() ? 'https://' : 'http://')
            .sprintf(Mage::getStoreConfig(self::XML_SEVERITY_ICONS_URL_PATH), Mage::getVersion(), 'SEVERITY_'.$icon_type);
        //------------------------------------------------------------------------------------------------------------------
        return $url;
    }

    /*
     * Render columns with our new one
     * Also, add buttons "Link all presentations" and "Configuration"
     */
    protected function _prepareColumns() {
        //------------------------------------------------------------------------------------------------------------------
        $store = $this->_getStore();
        $request = Mage::app()->getRequest();
        $cookies = Mage::getModel('core/cookie');
        //------------------------------------------------------------------------------------------------------------------
        $observer = Mage::getSingleton('selfhosted/observer');
        $observer->ExtendOrbitvu();

        $orbitvu_connected = $observer->_Orbitvu->IsConnected();
        //------------------------------------------------------------------------------------------------------------------
        $o_configuration_url = explode('/system_config/', $this->getUrl('*/system_config/edit/section/orbitvu_selfhosted/'));
        $o_configuration_url = '*/system_config/'.$o_configuration_url[count($o_configuration_url)-1];

        $o_update_url = explode('/catalog_product/', $this->getUrl('*/catalog_product/index/selfhosted/update'));
        $o_update_url = '*/catalog_product/'.$o_update_url[count($o_update_url)-1];

        $o_close_url = explode('/catalog_product/', $this->getUrl('*/catalog_product/index/selfhosted/dismiss'));
        $o_close_url = '*/catalog_product/'.$o_close_url[count($o_close_url)-1];

        $o_close_welcome_url = explode('/catalog_product/', $this->getUrl('*/catalog_product/index/selfhosted/dismiss_welcome'));
        $o_close_welcome_url = '*/catalog_product/'.$o_close_welcome_url[count($o_close_welcome_url)-1];

        $display_first_time = true;
        //------------------------------------------------------------------------------------------------------------------

        /*
         * Add column Orbitvu
         */
        //------------------------------------------------------------------------------------------------------------------
        $this->addColumnAfter('orbitvu_selfhosted', array(
            'header' => Mage::helper('catalog')->__('Orbitvu'),
            'type' => 'text',
            'width' => '100px',
            'renderer' => 'Orbitvu_SelfHosted_Block_Adminhtml_Catalog_Product_Gridrenderer',
            'index' => 'orbitvu_selfhosted',
        ), 'sku'
        );
        //------------------------------------------------------------------------------------------------------------------

        /**
         * Install viewers
         */
        //------------------------------------------------------------------------------------------------------------------
        // if ($observer->_Orbitvu->GetConfiguration('viewer_previous') != $observer->_Orbitvu->GetConfiguration('viewer')) {
        // try {
        // $viewers = $observer->_Orbitvu->Connect->InstallViewer($observer->_Orbitvu->GetConfiguration('viewer'));

        // $observer->_Orbitvu->UpdateConfiguration('viewer_previous', $observer->_Orbitvu->GetConfiguration('viewer'));
        // $observer->_Orbitvu->UpdateConfiguration('viewer', $observer->_Orbitvu->GetConfiguration('viewer'));
        // }
        // catch (Exception $e) {
        // $observer->_Orbitvu->UpdateConfiguration('viewer', $observer->_Orbitvu->GetConfiguration('viewer_previous'));
        //------------------------------------------------------------------------------------------------------------------
        //$buttons = '<a href="'.$this->getUrl('*/catalog_product/index/').'" onclick="closeMessagePopup();">'.Mage::helper('catalog')->__('OK').'</a>';
        //------------------------------------------------------------------------------------------------------------------
        // $orbitvu_popup = $this->genPopup(
        // Mage::helper('catalog')->__('Error!'),
        // Mage::helper('catalog')->__('Error while downloading Orbitvu Viewer! Try to follow the instruction below or contact Orbitvu (dev@orbitvu.com) if you still need help.').'<br />'.$e->getMessage(),
        // $buttons,
        // '*/catalog_product/index/',
        // 'MAJOR'
        //);
        //------------------------------------------------------------------------------------------------------------------
        //$display_first_time = false;
        //------------------------------------------------------------------------------------------------------------------
        //}
        //}
        //------------------------------------------------------------------------------------------------------------------

        /*
         * Synchronize actions
         */
        //------------------------------------------------------------------------------------------------------------------
        if (date('Y-m-d', strtotime($observer->_Orbitvu->GetConfiguration('last_refreshed'))) != date('Y-m-d')) {
            $observer->_Orbitvu->SynchronizePresentationsItems();
        }
        //------------------------------------------------------------------------------------------------------------------
        if ($request->getParam('selfhosted') == 'update') {
            $observer->SynchronizeAllProducts();
        }
        //------------------------------------------------------------------------------------------------------------------


        /*
         * Render Orbitvu buttons actions
         */
        //------------------------------------------------------------------------------------------------------------------
        $this->addRssList(
            '></a>',
            '<span style="display: inline-block; position: relative; width: 1px; height: 16px; background: white;"><span style="position: absolute; left: -20px; top: 0; width: 20px; background: white; height: 20px; cursor: default;" onclick="return false;"></span></span><a href="javascript:if (confirm(\''.Mage::helper('catalog')->__('Do you really want to link uploaded presentations to all products matched with SKU or name?').' '.Mage::helper('catalog')->__('This operation will only be applied to not yet linked products.').'\')) { window.location.href = \''.$this->getUrl($o_update_url).'\'; }"><button type="button" class="scalable orbitvu-button" title="'.Mage::helper('catalog')->__('Link uploaded presentations to all products matched with SKU or name.').' '.Mage::helper('catalog')->__('This operation will only be applied to not yet linked products.').'" id="button_orbitvu_update"><span><span style="float: left; width: 16px; height: 16px; margin: 0 4px 0 0 !important; background-image: url('.Mage::getBaseUrl('media').'orbitvu/white.png) !important; background-repeat: no-repeat; background-position: 0 -108px !important;"></span>'.Mage::helper('catalog')->__('Link Orbitvu presentations').'</span></button>'.(!$orbitvu_connected ? '<a href="#">' : '').'');
        //------------------------------------------------------------------------------------------------------------------
        $this->addRssList($o_configuration_url, '<button type="button" class="scalable orbitvu-button" title="'.Mage::helper('catalog')->__('Orbitvu Configuration').'" id="button_orbitvu_configuration"><span style="float: left; width: 16px !important; height: 16px !important; margin: 0 5px 0 0; padding: 0; background-image: url('.Mage::getBaseUrl('media').'orbitvu/white.png); background-repeat: no-repeat; background-position: -64px -108px;"></span><span>'.$this->__('Configuration').'<span></span></span></button><script>document.getElementById(\'button_orbitvu_configuration\').parentNode.className = \'\';document.getElementById(\'button_orbitvu_update\').parentNode.className = \'\';
    window.onbeforeunload = function() {
        document.getElementById(\'orbitvu_postloader\').style.display = \'block\';
    }            
</script></a><div id="orbitvu_postloader">
    <div id="orbitvu_postloader_bg"></div>
    <div id="orbitvu_postloader_fg"><img src="'.Mage::getBaseUrl('media').'orbitvu/loader.gif" style="margin-top: 150px;" alt="" /></div>
</div><style type="text/css">
            #orbitvu_postloader { display: none; }
    #orbitvu_postloader_bg { position: fixed; width: 100%; height: 100%; top: 0; left: 0; z-index: 999998; background: white; opacity: 0.8; }
    #orbitvu_postloader_fg { position: fixed; width: 100%; top: 0; left: 0; z-index: 999999; text-align: center; }
.orbitvu-button { border-color: #1191A6; background: #8dd5e0; background: -moz-linear-gradient(top,  #8dd5e0 0%, #009cb5 45%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#8dd5e0), color-stop(45%,#009cb5)); background: -webkit-linear-gradient(top,  #8dd5e0 0%,#009cb5 45%); background: -o-linear-gradient(top,  #8dd5e0 0%,#009cb5 45%); background: -ms-linear-gradient(top,  #8dd5e0 0%,#009cb5 45%); background: linear-gradient(to bottom,  #8dd5e0 0%,#009cb5 45%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#8dd5e0\', endColorstr=\'#009cb5\',GradientType=0 ); }
.orbitvu-button:hover { background: #ddfaff; background: -moz-linear-gradient(top,  #ddfaff 0%, #15adc6 59%); background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ddfaff), color-stop(59%,#15adc6)); background: -webkit-linear-gradient(top,  #ddfaff 0%,#15adc6 59%); background: -o-linear-gradient(top,  #ddfaff 0%,#15adc6 59%);background: -ms-linear-gradient(top,  #ddfaff 0%,#15adc6 59%); background: linear-gradient(to bottom,  #ddfaff 0%,#15adc6 59%); filter: progid:DXImageTransform.Microsoft.gradient( startColorstr=\'#ddfaff\', endColorstr=\'#15adc6\',GradientType=0 ); }
</style><a href="#">');
        //------------------------------------------------------------------------------------------------------------------
        return parent::_prepareColumns();
        //------------------------------------------------------------------------------------------------------------------
    }

}

?>