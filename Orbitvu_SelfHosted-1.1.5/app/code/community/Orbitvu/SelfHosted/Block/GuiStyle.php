<?php
/**
 * @category    Orbitvu
 * @package     Orbitvu_SelfHosted
 * @copyright   Copyright (C) 2014 Orbitvu (http://www.orbitvu.com)
 * @license     http://www.orbitvu.com/plugins/license
 * @version     1.0.0
 */

class Orbitvu_SelfHosted_Block_GuiStyle
{
    
    /*
     * Options list
     */
    public function OrbitvuGuiStyleOptions() {
        return array(
            '0'        => Mage::helper('adminhtml')->__('Default round style'),
            '1'              => Mage::helper('adminhtml')->__('Square style'),
            '2'            => Mage::helper('adminhtml')->__('Round style with rotation scroll'),
            '3'       => Mage::helper('adminhtml')->__('Square style with rotation scroll'),
            '4'               => Mage::helper('adminhtml')->__('No buttons at all')
        );
    }
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        $array = $this->OrbitvuGuiStyleOptions();
        
        $ret = array();
        foreach ($array as $key => $value) {
            $ret[] = array(
                'value' => $key,
                'label' => $value               
            );
        }
        
        return $ret;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        return $this->OrbitvuGuiStyleOptions();
    }
}
?>