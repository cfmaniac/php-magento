<?php
/**
 * @category    Orbitvu
 * @package     Orbitvu_SelfHosted
 * @copyright   Copyright (C) 2014 Orbitvu (http://www.orbitvu.com)
 * @license     http://www.orbitvu.com/plugins/license
 * @version     1.0.0
 */

class Orbitvu_SelfHosted_Block_Remoteurl
{
    /**
     * New input
     * @return type string
     */
    public function getCommentText() { 
        
        $observer = Mage::getSingleton('selfhosted/observer');
        $observer->ExtendOrbitvu();

        if (isset($_GET['new_local_session_key'])) {
            $key = md5(md5($_SERVER['SERVER_SOFTWARE'].$_SERVER['SERVER_ADDR'].$_SERVER['HTTP_HOST'].$_SERVER['PATH'].date('Y-m-d H:i:s').' '.mt_rand(1000, 10000)));
            $observer->_Orbitvu->UpdateConfiguration('local_session_key', $key);
            return Mage::app()->getResponse()->setRedirect(Mage::helper('adminhtml')->getUrl("adminhtml/system_config/edit/", array('section'=>'orbitvu_selfhosted')));

        }

        return '<div style="position:relative;height:20px;"><div style="position: absolute;top:0;left:-5px;"><input type="text" value="'.$observer->_Orbitvu->GetRemoteAuthorizationUrl().'" style="width: 600px;font-size: 11px;" readonly="readonly" /></div></div> <p class="note">'.Mage::helper('catalog')->__('Enter this URL to your ORBITVU/ALPHASHOT EDITOR. You will be able to upload *.ovus files from your Orbitvu Editor directly!').' <a href="'.Mage::helper('adminhtml')->getUrl('*/system_config/edit/section/orbitvu_selfhosted').'?new_local_session_key" onclick="return confirm(\''.Mage::helper('catalog')->__('Don\\\'t forget to change new generated url in your ORBITVU/ALPHASHOT EDITOR').'\');">'.Mage::helper('catalog')->__('Generate new url').'</a></p>';
    }
}
?>