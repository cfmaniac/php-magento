<?php
/**
 * @category    Orbitvu
 * @package     Orbitvu_SelfHosted
 * @copyright   Copyright (C) 2014 Orbitvu (http://www.orbitvu.com)
 * @license     http://www.orbitvu.com/plugins/license
 * @version     1.0.0
 */

class Orbitvu_SelfHosted_Block_Viewer {
    
    /*
     * Options list
     */
    public function OrbitvuViewerOptions() {
        //------------------------------------------------------------------------------------------------------- 
        $observer = Mage::getSingleton('selfhosted/observer');
        $_Orbitvu = $observer->ExtendOrbitvu();
        //------------------------------------------------------------------------------------------------------- 
        $viewers = $_Orbitvu->Connect->CallSUN('viewers/licenses');
        
        foreach ($viewers as $viewer) {
            $out[$viewer->type] = $viewer->type.' '.$viewer->available_version;
        }
        
        return $out;
        //------------------------------------------------------------------------------------------------------- 
    }
    
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray() {
        $array = $this->OrbitvuViewerOptions();
        
        $ret = array();
        foreach ($array as $key => $value) {
            $ret[] = array(
                'value' => $key,
                'label' => $value               
            );
        }
        
        return $ret;
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray() {
        return $this->OrbitvuViewerOptions();
    }
}

?>