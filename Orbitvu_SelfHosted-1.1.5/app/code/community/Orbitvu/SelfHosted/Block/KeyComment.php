<?php
/**
 * @category    Orbitvu
 * @package     Orbitvu_SelfHosted
 * @copyright   Copyright (C) 2014 Orbitvu (http://www.orbitvu.com)
 * @license     http://www.orbitvu.com/plugins/license
 * @version     1.0.0
 */

class Orbitvu_SelfHosted_Block_KeyComment
{
    /**
     * Parsed comment text
     * @return type string
     */
    public function getCommentText() { 
        return Mage::helper('catalog')->__('You need <a href="http://orbitvu.co" target="_blank">Orbitvu SUN</a> account and License Key to use Orbitvu extension.').'<br />'.'<a href="'.Mage::helper('adminhtml')->getUrl('*/catalog_product/index/selfhosted/show_welcome').'">'.Mage::helper('catalog')->__('You can register').'</a> '.Mage::helper('catalog')->__(' a trial account').'.';
    }
}
?>